## Debugging clojure - How to?


At the last Clojure Unconference in London (24/07/2014) we had a session about Debugging in Clojure. Here is a summary of all knowledge that was shared during this session, plus what I searched afterwards to fill in the gaps.

We covered:

 - debugging libraries
 - methodologies
 - tools
 - useful tips

I'm up for keeping this page up to date if you send me issues and pull requests.


## Helpful debugging libraries

- [vinyasa](https://github.com/zcaudate/vinyasa) is not a debugging library, but it will let you inject all your commonly used debugging functions in a namespace that you can then use from anywhere: any REPL, any source code. The idea is: bring your debugging toolbelt everywhere with you. 

- In [clojure.tools.trace](https://github.com/clojure/tools.trace) there is the awesome `deftrace`: replace any `defn` in your code with `deftrace`, and for that function, every call will be printed (name, arguments, result). Nested function calls are indented, so you get a very clear trace of what happened.   
`trace-forms` will tell you which form has thrown an exception in your code. And there are more treasures to use in this library.   
Some of these functions are presented in [this blog post](http://deliberate-software.com/clojure-debugger/) where the author says they are more valuable than a debugger.

- [let-def](http://www.learningclojure.com/2010/09/astonishing-macro-of-narayan-singhal.html) is a macro that helps debug let statements. Very handy!

- The `c` function in [mascip.debug](https://bitbucket.org/mascip/mascip.debug) turns comments into debug statements, to debug `(->> threading chains)`. There's also `dbg` in this library, to debug `(long (chains (of (composed (functions)))))`.

- [print-foo](https://github.com/AlexBaranosky/print-foo) is another debugging library which seems to be used by many people (more than 4,000 downloads): "Just add 'print-' to the front of a normal `->>`, `->`, `let`, `defn`, `defn-`, `cond`, `if` and prints useful information."

- [spyscope](https://github.com/dgrnbrg/spyscope) seems pretty powerful, I haven't explored it much; if you did, would you write a short description to share here?



## Debugging Methodologies

Someone said that they use [`with-redefs`](http://clojuredocs.org/clojure_core/clojure.core/with-redefs) in the REPL to temporarily change the environment (while debugging) and reduce the unknown.   
**TODO**: any link to a relevant blog post or video that shows this technique in action? I have not used this method myself. I will try it but would be  and am very curious.

Use logging tools. [Timbre](https://github.com/ptaoussanis/timbre) has more features than [clojure.tools.logging](https://github.com/clojure/tools.logging). Someone mentionned the idea of using [prismatic/graph](https://github.com/Prismatic/plumbing) for logging; **TODO**: anything on that?

**TODO**: "REPL connected to prod server continuously, just plug into it". I don't think it was used for debugging though, I didn't catch the details. Anyone?


## Debugging Tools

According to [this video](https://www.youtube.com/watch?v=sA5zOLCa3Xw), historically there was [debug-repl](https://github.com/georgejahad/debug-repl), followed by [swank-clojure](https://github.com/technomancy/swank-clojure), and then [CDT](http://georgejahad.com/clojure/cdt.html). 

- debug-repl lets you put break statements into your code, where the repl will stop and give you a prompt in a sub-repl. From there you can use local variables and do pretty much anything.

- swank-clojure provides IDE-integration (both for Vim and Emacs - see relevant sections below) with breakpoints and many keyboard bindings. In Emacs, it is deprecated in favor of Cider (read below).

- CDT lets you "break on exception", which means you will know where exactly the exception was thrown, and be able to access the local variables at that point, to better diagnose what happened.

But this was all before the [nREPL](https://github.com/clojure/tools.nrepl), which provides a REPL server and client.

Built on top of nREPL I have found [redl](https://github.com/dgrnbrg/redl) which is a debug REPL, and [Ritz](https://github.com/pallet/ritz) which is a collection of libraries (middleware, debugger, utils, etc) built on the [Cider](https://github.com/clojure-emacs/cider) nREPL client. More on these two in the Vim and Emacs sections below.

So let's dive into editor specific tools!

(disclaimer: I'm a Vim user. I've put the most basic info for other editors here, and I count on your pull requests to add more info for other editors)


# Vim

[fireplace.vim](https://github.com/tpope/vim-fireplace) should do pretty much everything you want. It was written by Tim Pope ,who you might know for his many other awesome Vim modules, so you can count on this plugin to be well maintained. It's also referenced everywhere in the Clojure doc, as the goto for Vim users. Its most powerful feature, I think, is to let you execute any form in your code with a keybinding. There is a short (3min) and [very cool video](https://www.youtube.com/watch?v=LiA56W3V3_w) that shows you how to use this for an integrated workflow. Simple and impressive.

I have found fireplace.vim recommended over the older [slimv.vim](http://www.vim.org/scripts/script.php?script_id=2531)+swank [in](https://www.deepbluelambda.org/programming/clojure/programming-clojure-with-vim) [several](http://www.reddit.com/r/vim/comments/1fa2xk/getting_vim_lein_and_swank_to_play_nicely/) [places](http://adambard.com/blog/quickstart-clojure-on-vim-lein-slimv-windows/). However, slimv.vim could be your choice if you wanted to use Ritz, and because it currently has more features than fireplace.vim; I have heard it has more bugs and glitches though; the difference is explained [here](https://github.com/dgrnbrg/vim-redl/issues/26#event-148335908). If you are used to swank and slimv or if you plan to use these for other Lisp dialects, that seems to be a good option. If you only plan on using it for Clojure, then you're probably better off with fireplace.vim.

Another possibility instead of Ritz, is to use [vim-redl](https://github.com/dgrnbrg/vim-redl) which complements fireplace.vim beautifully (the authors of these two modules seem to be interacting through Github). vim-redl is a debug REPL inside of Vim. It means that you get all the power of Vim inside the REPL:

 - use all your Vim keybindings and plugins (like [paredit](https://github.com/vim-scripts/paredit.vim)) to write in this REPL and navigate it, Do/Undo, etc: you are in Vim;
 - get vim highlighting for free in the REPL;
 - copy-paste anything between your code buffer and REPL buffer, or within the REPL itself;
 - It also has a fuzzy completion engine,
 - and lets you set `break`, `continue`, and `step-out` statements which work in core.async. When you `break` you get a sub-REPL where you can access all local variables. You can then `step-out`, `continue`, or `(continue some-result)` which will send `some-result` to the code.   

It kind of feels like the boudary between the text editor, the REPL and a debugger has disappeared: they are now all embedded into Vim.

(I didn't mention vim-redl at the unconference because I found it today while writing this summary)
**TODO**: I could not find any video to demonstrate what vim-repl can do. Anyone?

Unfortunately vim-redl does not have all the capabilities of Ritz, but it's pretty awesome already.
If you really really wanted to use Ritz, you would have to:

- use slimv.vim instead of fireplace.vim. I haven't tried this option - please let me know if you do, and how easy/hard it was to set up.
- make fireplace.vim be able to connect to a Ritz nREPL.
- switch to Emacs, possibly with the help of the [Evil](http://www.emacswiki.org/emacs/Evil) plugin, which lets you use Vim keymappings to edit text. But will also need to know how to use Emacs, so that's obviously more work. You will find several blog posts online of people explaning how they did this switch.

# Emacs

Ritz is a debugging nREPL which lets you `break` & `continue`, break on exception (and see the local variables at any point in the stack trace, and much more. I found [a video](http://www.infoq.com/presentations/ritz-clojure) about it, and some [slides](http://hugoduncan.org/ritz-conj). The live demo is at [21:30 in the video](http://youtu.be/sA5zOLCa3Xw?t=21m30s).


**TODO**: more links and explanations?

# Lightable

A REPL is integrated within this IDE, right?


# Eclipse

[Counterclockwise](http://code.google.com/p/counterclockwise/) is Eclipse's plugin for Clojure. It lets you set breakpoints. I have read it being recommended in multiple places.


# Other

I found [schmetterling](https://github.com/prismofeverything/schmetterling) to debug clojure from the browser, which means it's non IDE dependent. It does not use nREPL though.

IntelliJ is a Java editor which lets you set breakpoints (does not work with core.async).

## Refactoring

One other possible reason to switch from Vim to Emacs, is [clj-refactor.el](https://github.com/clojure-emacs/clj-refactor.el), which provides binding to perform common refactorings.

Some projects might be worth watching in the future:
- [Poker](https://github.com/ctford/poker) aims to provide refactoring for Vim, but hasn't been updated in a year.
- [sjacket](https://github.com/cgrand/sjacket) aims at being a commong backend for Clojure editors.
- [nrepl-refactor](https://github.com/cgag/nrepl-refactor) is an nREPL middleware for refactoring. According to the doc, it could be used with fireplace.vim already. Please let me know if you try.

I think the reason why Emacs has more than Vim at the moment, is that its scripting language is a Lisp, which makes Clojure programmers more likely to want to play with it. Hopefully, more and more editor features will be provided as nREPL middleware in the future, so that it can be used in all editors.


## Useful tips

#### Use `#_`
Don't forget: you can always "comment out" a function by adding `#_` in front of it:

    (-> 1 #_(* 3) inc)
    => 2

It could be useful to setup a shortcut for inserting that in the code (these two keys are a bit awkward).

#### One `#_` trick
For commenting out, say, complete cond clauses, is that you can write `#_` multiple times in succession to ignore the next n forms so that

    [1 2 3 #_ #_ 4 5 6]
    => [1 2 3 6]

    (let [n some-number]
      (cond
        (< n 0) "too small"
        #_#_(> n 6) "too big"
        :else "pefect!)

#### Print inside `let`
Inside a `let` statements you can also debug like this:

    (let [data-to-debug (inc 1)
          _ (dbg data-to-debug))
    => dbg: data-to-debug => 2

I used the `dbg` function from [mascip.debug](https://bitbucket.org/mascip/mascip.debug) here, but you could use `prn` or `println` instead.


## Other Resources

[this article](http://dev.solita.fi/2014/03/18/pimp-my-repl.html) show you how to pimp up your REPL, it's full of cool tools. Thought it could be useful. Beware, the vinyasa API has changed a bit since this article was published.


## Acknowledgement

Everyone at this London Unconference, thank you for this event, I'm looking forward to the next one.

For those who haven't yet, you should come to a Clojure [coding dojo](http://londonclojurians.org/). People do exercises of all levels, and I find that I learn a lot by working on the same project with other people, while sharing ideas and looking at each other's code and toolset.

Acknowledgements for pull requests will go here.

Thank you :)    
to Daniel Barlow,
